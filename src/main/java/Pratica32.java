/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Gliceu Camargo <gliceu@utfpr.edu.br>
 */
public class Pratica32 
{
    public static void main(String[] args) {
       double desvio = 3; double media = 67;  double x = -1;          
           double d = densidade(x,media,desvio);//chama o método densidade(fórmula).
             System.out.println(d);//imprime o resultado da operação.
    }
     public static double densidade(double x, double media, double desvio) { //Método densidade com argumentos repassados
          double d =  1/(Math.sqrt(2*Math.PI)*desvio)*Math.pow(Math.exp(1), -.5*(Math.pow((x-media)/desvio, 2)));
            return d;
      }
}
